import { useNavigation, useRoute } from '@react-navigation/core'
import React, { useState } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import screens from '../navigation/screens'

interface Props {}

export const AuthorDetails = (props: Props) => {
    const [author, setAuthor] = useState<any>(null)
    const route = useRoute<any>()
    const navigation = useNavigation()

    console.log('route', route)

    React.useEffect(() => {
        requestAuthorAsync()
    }, [])

    const requestAuthorAsync = async () => {
        try {
            const requestUrl = `https://jsonplaceholder.typicode.com/users/${route.params?.authorId}`
            const res = await fetch(requestUrl)
            const data = await res.json()

            setAuthor(data)
        } catch (error) {
            console.log(error)
        }
    }

    // if (author === null) {
    //     return <Text>loading...</Text>
    // }

    const onAuthorClick = () => {
        console.log('onAuthorClick')
        navigation.navigate(screens.POSTS_AUTHOR_SCREEN)
    }

    return (
        <View style={styles.container}>
            {author === null ? (
                <Text>loading...</Text>
            ) : (
                <TouchableOpacity onPress={onAuthorClick}>
                    <Text>{author.name}</Text>
                    <Text>{author.phone}</Text>
                </TouchableOpacity>
            )}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 50,
        padding: 5,
        marginTop: 20,
        backgroundColor: 'orange'
    }
})
