import { useNavigation } from '@react-navigation/core'
import React from 'react'
import { View, Text, StyleSheet, Button } from 'react-native'
import screens from '../navigation/screens'

interface Props {
    title: string
    description?: string
    authorId: number
}

export const Card = (props: Props) => {
    const navigation = useNavigation()

    const readMore = () => {
        console.log('Go to post details')
        navigation.navigate(screens.POSTS_DETAILS_SCREEN, {
            title: props.title,
            body: props.description,
            authorId: props.authorId
        })
    }

    return (
        <View style={styles.cardContainer}>
            <Text numberOfLines={1} style={styles.cardTitle}>
                {props.title || '-'} {props.authorId}
            </Text>
            <Text numberOfLines={3} style={styles.cardDesc}>
                {props.description || '-'}
            </Text>
            <Button onPress={readMore} title="Read more" />
        </View>
    )
}

const styles = StyleSheet.create({
    cardContainer: {
        backgroundColor: 'yellow',
        margin: 10
    },
    cardTitle: {
        fontSize: 20,
        padding: 10,
        backgroundColor: 'yellow'
    },
    cardDesc: {
        paddingVertical: 5,
        paddingHorizontal: 10,
        backgroundColor: 'orange',
        fontVariant: ['small-caps']
    }
})
