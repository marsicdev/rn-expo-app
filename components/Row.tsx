import * as React from 'react'
import { View, StyleSheet } from 'react-native'

export default function Row(props: any) {
    return <View style={styles.rowContainer}>{props.children}</View>
}

const styles = StyleSheet.create({
    rowContainer: {
        flexDirection: 'row',
        backgroundColor: 'white',
        justifyContent: 'space-between',
        margin: 10
    }
})
