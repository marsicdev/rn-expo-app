import React, { useState } from 'react'
// import { SearchBar as RNESearchBar } from 'react-native-elements'
import { SearchBar } from 'react-native-elements'

const TopSearchBar = () => {
    const [search, setSearch] = useState('')

    const updateSearch = (search: string) => {
        setSearch(search)
    }

    return <SearchBar onChangeText={updateSearch} value={search} />
}

export default TopSearchBar
