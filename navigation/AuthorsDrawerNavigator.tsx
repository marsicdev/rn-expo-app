import React from 'react'
import { createDrawerNavigator } from '@react-navigation/drawer'
import AuthorsScreen from '../screens/authors/AuthorsScreen'
import NotFoundScreen from '../screens/NotFoundScreen'
import { PhotosStackNavigator } from './PhotosStackNavigator'

const Drawer = createDrawerNavigator()

function AuthorsDrawerNavigator() {
    return (
        <Drawer.Navigator>
            <Drawer.Screen name="Authors" component={AuthorsScreen} />
            <Drawer.Screen name="Albums" component={NotFoundScreen} />
            <Drawer.Screen name="Photos" component={PhotosStackNavigator} />
        </Drawer.Navigator>
    )
}

export default AuthorsDrawerNavigator
