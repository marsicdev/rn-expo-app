import { Ionicons } from '@expo/vector-icons'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator } from '@react-navigation/stack'
import * as React from 'react'

import Colors from '../constants/Colors'
import useColorScheme from '../hooks/useColorScheme'
import { AuthorDetailsScreen } from '../screens/AuthorDetailsScreen'
import NotFoundScreen from '../screens/NotFoundScreen'
import PostDetailsScreen from '../screens/PostDetailsScreen'
import TabOneScreen from '../screens/TabOneScreen'
import TabTwoScreen from '../screens/TabTwoScreen'
import screens from './screens'
import AuthorsScreen from '../screens/authors/AuthorsScreen'
import AuthorsDrawerNavigator from './AuthorsDrawerNavigator'

const BottomTab = createBottomTabNavigator<any>()

export default function BottomTabNavigator() {
    const colorScheme = useColorScheme()

    return (
        <BottomTab.Navigator
            initialRouteName={screens.POSTS_TAB}
            tabBarOptions={{ activeTintColor: Colors[colorScheme].tint }}>
            <BottomTab.Screen
                name={screens.POSTS_TAB}
                component={TabOneNavigator}
                options={{
                    title: 'Posts',
                    tabBarIcon: ({ color }) => <TabBarIcon name="ios-code" color={color} />
                }}
            />
            <BottomTab.Screen
                name={screens.ABOUT_TAB}
                component={TabTwoNavigator}
                options={{
                    title: 'About',
                    tabBarIcon: ({ color }) => (
                        <TabBarIcon name="information-circle-outline" color={color} />
                    )
                }}
            />
            <BottomTab.Screen
                name={screens.AUTHORS_TAB}
                component={AuthorsDrawerNavigator}
                options={{
                    title: 'Authors',
                    tabBarIcon: ({ color }) => <TabBarIcon name="person" color={color} />
                }}
            />
        </BottomTab.Navigator>
    )
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
function TabBarIcon(props: { name: React.ComponentProps<typeof Ionicons>['name']; color: string }) {
    return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const TabOneStack = createStackNavigator()

function TabOneNavigator() {
    return (
        <TabOneStack.Navigator>
            <TabOneStack.Screen
                name={screens.POSTS_SCREEN}
                component={TabOneScreen}
                options={{ headerTitle: 'Posts List' }}
            />
            <TabOneStack.Screen
                name={screens.POSTS_DETAILS_SCREEN}
                component={PostDetailsScreen}
                options={{ headerTitle: 'Posts details' }}
            />
            <TabOneStack.Screen
                name={screens.POSTS_AUTHOR_SCREEN}
                component={AuthorDetailsScreen}
                options={{ headerTitle: 'Posts details' }}
            />
        </TabOneStack.Navigator>
    )
}

const TabTwoStack = createStackNavigator()

function TabTwoNavigator() {
    return (
        <TabTwoStack.Navigator>
            <TabTwoStack.Screen
                name="TabTwoScreen"
                component={TabTwoScreen}
                options={{ headerTitle: 'Tab Two Title' }}
            />
        </TabTwoStack.Navigator>
    )
}

// const Drawer = createDrawerNavigator()

// function AuthorsDrawerNavigator() {
//     return (
//         <Drawer.Navigator>
//             <Drawer.Screen name="Authors" component={AuthorsScreen} />
//             <Drawer.Screen name="Albums" component={NotFoundScreen} />
//             <Drawer.Screen name="Photos" component={NotFoundScreen} />
//         </Drawer.Navigator>
//     )
// }
