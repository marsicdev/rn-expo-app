import { createStackNavigator } from '@react-navigation/stack'
import React from 'react'
import PhotosScreen from '../screens/authors/PhotosScreen'

interface Props {}

const PhotosNavigator = createStackNavigator()

export const PhotosStackNavigator = (props: Props) => {
    return (
        <PhotosNavigator.Navigator initialRouteName="photos">
            <PhotosNavigator.Screen name={'photos'} component={PhotosScreen} />
        </PhotosNavigator.Navigator>
    )
}
