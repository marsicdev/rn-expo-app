import * as React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { AuthorDetails } from '../components/AuthorDetails'

export default function PostDetailsScreen(props: any) {
    console.log('props', props.navigation)
    console.log('props', props.route.params)

    if (props.route.params === undefined) {
        return <Text>Nothing here</Text>
    }
    // const { title, body } = props.route.params

    return (
        <View style={styles.container}>
            <Text style={styles.title}>{props.route.params.title}</Text>
            <Text>{props.route.params.body}</Text>
            <AuthorDetails /*authorId={props.route.params.authorId} */ />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
        padding: 20
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold'
    }
})
