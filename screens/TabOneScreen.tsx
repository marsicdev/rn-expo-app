import * as React from 'react'
import { FlatList, Text } from 'react-native'
import { Card } from '../components/Card'

export default function TabOneScreen() {
    const [posts, setPosts] = React.useState([])

    React.useEffect(() => {
        requestPostsAsync()
    }, [])

    const requestPostsAsync = async () => {
        const res = await fetch('https://jsonplaceholder.typicode.com/posts')
        const data = await res.json()

        setPosts(data)
    }

    const renderCard = ({ item }: any) => (
        <Card title={item.title} description={item.body} authorId={item.userId} />
    )

    return (
        <FlatList
            data={posts}
            renderItem={renderCard}
            keyExtractor={(item: any) => item.id.toString()}
            // ListEmptyComponent={() => <Text>List empty</Text>}
        />
    )
}
