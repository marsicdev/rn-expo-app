import * as React from 'react'
import { View, Text, Image, ScrollView, TextInput, StyleSheet, Button } from 'react-native'
import { Card } from '../components/Card'
import Row from '../components/Row'

export default function TabTwoScreen() {
    const [counter, setCounter] = React.useState(0)

    const handleClick = () => {
        const newCounter = counter + 1
        setCounter(newCounter)
    }

    const data = [
        {
            title: 'Card1',
            description: 'Desc 1'
        },
        {
            title: 'Card2',
            description: 'Desc 2'
        },
        {
            title: 'Card3',
            description: 'Desc 3'
        }
    ]

    return (
        <View style={styles.container}>
            <ScrollView>
                {data.map(card => (
                    <Card title={card.title} description={card.description} />
                ))}
                {/* <View style={styles.card2}></View> */}
                <Row>
                    <View style={styles.box}></View>
                    <View style={styles.box}>
                        <Text style={styles.boxText}>{counter}</Text>
                    </View>
                    <View style={styles.box}></View>
                </Row>
                <Button title="Click me" onPress={handleClick} color="green" />
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'red'
    },

    boxText: {
        textAlign: 'center',
        alignSelf: 'center',
        backgroundColor: 'orange',
        fontSize: 30,
        paddingVertical: 5,
        paddingHorizontal: 15
    },

    card2: {
        backgroundColor: 'blue',
        height: 100,
        margin: 10
    },
    box: {
        justifyContent: 'center',
        alignContent: 'center',
        backgroundColor: 'violet',
        height: 100,
        width: 100
    }
})
