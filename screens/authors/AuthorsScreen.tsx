import React, { useEffect, useRef, useState } from 'react'
import { ActivityIndicator, Text, View } from 'react-native'
import { ListItem, Badge, SearchBar, Image } from 'react-native-elements'
import { SafeAreaView } from 'react-native-safe-area-context'

interface Props {}

const AuthorsScreen = (props: Props) => {
    // const staticUsers = useRef().current
    const [backupUsers, setBackupUsers] = useState<any[] | null>(null)
    const [users, setUsers] = useState<any[] | null>(null)

    useEffect(function () {
        fetchUsers()
    }, [])

    const [search, setSearch] = useState('')

    const updateSearch = (search: string) => {
        setSearch(search)

        const myUsers = backupUsers?.filter((user: any) => {
            return user.name.includes(search)
        })
        setUsers(myUsers)
    }

    function fetchUsers() {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(res => res.json())
            .then(data => {
                // setBackupUsers(data)
                // temp = data
                setUsers(data)
            })
    }

    if (users == null) {
        return (
            <SafeAreaView>
                <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'red' }}>
                    <ActivityIndicator animating size="large" />
                </View>
            </SafeAreaView>
        )
    }

    const hasUsers = users.length === 0
    if (hasUsers) {
        return <Text>No items</Text>
    }

    return (
        <SafeAreaView>
            <SearchBar onChangeText={updateSearch} platform="default" value={search} />
            <Image
                source={{ uri: 'https://marsic.dev/images/marko-small.png' }}
                style={{ width: 200, height: 200 }}
                PlaceholderContent={<ActivityIndicator size="large" />}
            />
            <View>
                {users.map((user, i) => (
                    <ListItem key={i} bottomDivider>
                        {/* <Avatar source={{ uri: user.avatar_url }} /> */}
                        <ListItem.Content>
                            <ListItem.Title>{user.name}</ListItem.Title>
                            <ListItem.Subtitle>{user.email}</ListItem.Subtitle>
                        </ListItem.Content>
                        <Badge value={3} />
                    </ListItem>
                ))}
            </View>
        </SafeAreaView>
    )
}

export default AuthorsScreen
