import { StatusBar } from 'expo-status-bar'
import React, { useEffect, useState } from 'react'
import { FlatList, View } from 'react-native'
import { Avatar, ListItem, Text } from 'react-native-elements'

interface Props {}

const PhotosScreen = (props: Props) => {
    const [photos, setPhotos] = useState<any[] | null>(null)

    useEffect(function () {
        fetchUsers()
    }, [])

    function fetchUsers() {
        fetch('https://jsonplaceholder.typicode.com/photos')
            .then(res => res.json())
            .then(data => {
                setPhotos(data)
            })
    }

    const keyExtractor = (item: any, index: number) => index.toString()

    const renderItem = ({ item }: any) => (
        <ListItem bottomDivider>
            <Avatar source={{ uri: item?.thumbnailUrl }} />
            <ListItem.Content>
                <ListItem.Title>{item?.title}</ListItem.Title>
                <ListItem.Subtitle>{item?.url}</ListItem.Subtitle>
            </ListItem.Content>
            <ListItem.Chevron />
        </ListItem>
    )

    return (
        <View>
            <StatusBar style="light" />
            <FlatList
                ListHeaderComponent={<Text h2>My photos</Text>}
                ListHeaderComponentStyle={{
                    backgroundColor: 'white',
                    paddingVertical: 20,
                    paddingHorizontal: 10
                }}
                ListEmptyComponent={<Text>List is loading or empty</Text>}
                initialNumToRender={15}
                keyExtractor={keyExtractor}
                data={photos || []}
                refreshing
                onRefresh={() => {}}
                renderItem={renderItem}
            />
        </View>
    )
}

export default PhotosScreen
